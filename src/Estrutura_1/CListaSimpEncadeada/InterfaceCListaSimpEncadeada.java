package Estrutura_1.CListaSimpEncadeada;

import Estrutura_1.CListaDupEncadeadaComSentinela.IndiceForaDosLimitesException;
import Estrutura_1.CListaDupEncadeadaComSentinela.ItemNãoEncontradoException;
import Estrutura_1.CListaDupEncadeadaComSentinela.ListaVaziaException;

public interface InterfaceCListaSimpEncadeada<T>{
    void add(T elemento);
    void add(T elemento, int posicao)throws IndiceForaDosLimitesException;
    void remove(T elemento) throws ItemNãoEncontradoException, ListaVaziaException;
    void remove(int posicao) throws IndiceForaDosLimitesException, ListaVaziaException;
    int find(T elemento) throws ItemNãoEncontradoException;
    T get(int posicao) throws IndiceForaDosLimitesException;
    int count();
}
