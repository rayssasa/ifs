package Estrutura_1.PilhaComVetores;

public interface InterfacePilhaComVetor <T>{

    boolean isEmpty();
    boolean isFull();
    void push(T t)throws PilhaException;
    T pop() throws PilhaException;
    void peek() throws PilhaException;
}
