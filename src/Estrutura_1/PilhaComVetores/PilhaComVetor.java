package Estrutura_1.PilhaComVetores;

import java.util.Scanner;

public class PilhaComVetor <T> implements InterfacePilhaComVetor<T>{

    private T[] pilha;
    private int topo = -1;
    private int posicao;
    Scanner entrada = new Scanner(System.in);

//    public PilhaComVetor(){
//        size();
//    }
//
//    protected void size(){
//        this.pilha = (T[]) new Object[Integer.parseInt(JOptionPane.showInputDialog("Qual o tamanho da pilha: "))];
//    }

    public int size(){
        return this.posicao;
    }

    public PilhaComVetor(int capacidade) {
        this.pilha = (T[]) new Object[capacidade];
    }

    public boolean isEmpty(){
        if (this.topo == -1){
            return true;
        }
        return false;
    }

    public boolean isFull(){
        if(this.posicao == this.pilha.length){
            return true;
        }
        return false;
    }

    public void push(T elemento)throws PilhaException{
        if(isFull() == false){
            pilha[this.posicao++] = elemento;
            topo++;
        } else {
            throw new PilhaException("Estrutura_1.Pilha cheia!");
        }
    }

    public T pop() throws PilhaException {
        if (isEmpty() == false){
            posicao--;
            return pilha[topo--];
        } else {
            throw new PilhaException("Estrutura_1.Pilha vazia!");
        }
    }

    public void peek() throws PilhaException{
        if (isEmpty() == false){
            System.out.println(pilha[topo]);
        } else {
            throw new PilhaException("Estrutura_1.Pilha vazia!");
        }
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (int i=0; i<this.size()-1; i++){
            s.append(this.pilha[i]);
            s.append(", ");
        }
        if (this.size()>0){
            s.append(this.pilha[this.size()-1]);
        }
        s.append("]");
        return s.toString();
    }
}
