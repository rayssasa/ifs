package Estrutura_2;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import iojava.IOJson;
import iojava.IOObjeto;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Scanner;


public class Jogo implements Serializable {
    public static void main(String[] args){
        try {
            String path = "C:\\Users\\Rayssa\\IdeaProjects\\IFS\\src\\Estrutura_2\\jogo.json";
            IOJson iOJson = new IOJson(path);
            Type type = new TypeToken<Arvore<String>>(){}.getType();
//            Arvore<String> arvore = new Arvore<String>();
            Arvore<String> arvore = (Arvore<String>)iOJson.read(type);

            int opJogar;
            do {
                opJogar = JOptionPane.showConfirmDialog(null, "Jogar?", "Escolha", JOptionPane.YES_NO_OPTION);
                if (opJogar == JOptionPane.YES_OPTION) {
                    if (arvore.estaVazia()){
                        String pergunta = JOptionPane.showInputDialog("Entre com uma pergunta: ");
                        arvore.inserir(pergunta);
                        String respostaErrada = JOptionPane.showInputDialog("Entre com uma resposta incorreta: ");
                        try {
                            arvore.inserirEsq(arvore.raiz(), respostaErrada);
                        } catch (ArvoreVaziaException e){
                            System.out.println(e.getMessage());
                        }
                        String respostaCorreta = JOptionPane.showInputDialog("Entre com uma resposta correta: ");
                        try {
                            arvore.inserirDir(arvore.raiz(), respostaCorreta);
                        } catch (ArvoreVaziaException e){
                            System.out.println(e.getMessage());
                        }
                    } else {
                        int msgPense = JOptionPane.showConfirmDialog( null, "Pense em algo! Ok?", "", JOptionPane.OK_CANCEL_OPTION);
                        if (msgPense == JOptionPane.OK_OPTION){
                            try{
                                No atual = arvore.raiz();
                                while(!arvore.eInterno(atual)){
                                    int op = JOptionPane.showConfirmDialog(null, atual.getInfo(), "Escolha", JOptionPane.YES_NO_OPTION);
                                    if (op == JOptionPane.YES_OPTION) {
                                        atual = atual.getDir();
                                    } else {
                                        atual = atual.getEsq();
                                    }
                                }
                                int msgAdvinhada = JOptionPane.showConfirmDialog(null, "O que você pensou foi "+arvore.exibirAtual(atual)+"? ", "", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE);
                                if (msgAdvinhada == JOptionPane.YES_OPTION){
                                    JOptionPane.showMessageDialog(null, "Eu não erro!");
                                } else {
                                    JOptionPane.showMessageDialog(null, "A máquina errou! :( ");
                                    String pergunta2 = JOptionPane.showInputDialog("Entre com outra pergunta: ");
                                    arvore.inserirEsqTroca(atual,pergunta2);
                                    String resposta2 = JOptionPane.showInputDialog("Entre com uma resposta correta: ");
                                    arvore.inserirDir(atual, resposta2);
                                }
                            } catch (ArvoreVaziaException | InvalidPositionException e) {
                                System.out.println(e.getMessage());
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Jogo cancelado! :( ");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Fim do jogo!");
                }
            } while (opJogar == JOptionPane.YES_OPTION);

            iOJson.write(arvore);

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}